#include <petsc.h>

#undef __FUNCT__
#define __FUNCT__ "getEllIntegrals"
PetscErrorCode getEllIntegrals(PetscReal k, PetscReal h, PetscReal *E, PetscReal *K)
{
  PetscInt  N = 4/h;
  PetscInt max;
  PetscReal *xkVals, *wkVals;
  PetscReal xk, xl, xr, wk, val1, val2;
  PetscReal denom;
  
  PetscErrorCode ierr;
  PetscFunctionBeginUser;
  
  ierr = PetscMalloc1(sizeof(PetscReal)*N, &xkVals);CHKERRQ(ierr);
  ierr = PetscMalloc1(sizeof(PetscReal)*N, &wkVals);CHKERRQ(ierr);
  
  max = N;
  // initialize TS quadrature points. only right side necessary
  for(PetscInt i=0; i < N; ++i) {
    xkVals[i] = PetscTanhReal(.5*PETSC_PI*PetscSinhReal(i*h));
    denom = PetscCoshReal(.5*PETSC_PI*PetscSinhReal(i*h));
    wkVals[i] = .5*h*PETSC_PI*PetscCoshReal(i*h)/(denom*denom);
    if(xkVals[i] >= 1) {
      max = i;
      break;
    }
  }

  //integrate over points up to xkVals[max-1]
  PetscReal a = 0;
  PetscReal b = 1;
  PetscReal lina = (b-a)/2.0;
  PetscReal linb = (a+b)/2.0;
  // add center terms manually
  xk = lina*xkVals[0] + linb;
  if(E != NULL) {
    *E = 0;
    *E += PetscSqrtReal(1.0 - k*k*xk*xk)/PetscSqrtReal(1.0 - xk*xk);
    *E *= lina*wkVals[0];
  }
  if(K != NULL) {
    *K = 0;
    *K += 1.0/PetscSqrtReal((1-xk*xk)*(1-k*k*xk*xk));
    *K *= lina*wkVals[0];
  }
  for(PetscInt i=1; i < max; ++i) {
    xl = -1.0*lina*xkVals[i] + linb;
    xr = lina*xkVals[i] + linb;
    if(E != NULL) {
      val1 = wkVals[i]*lina*(PetscSqrtReal(1.0 - k*k*xl*xl)/PetscSqrtReal(1.0 - xl*xl));
      val2 = wkVals[i]*lina*(PetscSqrtReal(1.0 - k*k*xr*xr)/PetscSqrtReal(1.0 - xr*xr));
      if(val1 != INFINITY)
	*E += val1;
      if(val2 != INFINITY)
	*E += val2;
    }
    if(K != NULL) {
      val1 = wkVals[i]*lina/PetscSqrtReal((1-xl*xl)*(1-k*k*xl*xl));
      val2 = wkVals[i]*lina/PetscSqrtReal((1-xr*xr)*(1-k*k*xr*xr));
      if(val1 != INFINITY)
	*K += val1;
      if(val2 != INFINITY)
	*K += val2;
    }
  }

  ierr = PetscFree(xkVals);CHKERRQ(ierr);
  ierr = PetscFree(wkVals);CHKERRQ(ierr);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
  PetscReal E, K;
  PetscReal E_exact = 1.505941612360040352112;
  PetscReal K_exact = 1.639999865864511206865;

  PetscReal k = .4;

  PetscErrorCode ierr;
  ierr = PetscInitialize(&argc, &argv, NULL, NULL);CHKERRQ(ierr);
  ierr = PetscLogDefaultBegin();CHKERRQ(ierr);
  
  PetscReal h = .25; //step size, .25 seems to work well for these  
  ierr = getEllIntegrals(k, h, &E, &K);CHKERRQ(ierr);
  printf("E(%3.3f) = %15.15f\n", k, E);
  printf("error = %2.2e\n", PetscAbsReal(E-E_exact));
  printf("K(%3.3f) = %15.15f\n", k, K);
  printf("error = %2.2e\n", PetscAbsReal(K-K_exact));
  ierr = PetscFinalize();CHKERRQ(ierr);
}
