CFLAGS = -I.

all: ellquad.o
	${CLINKER} -o ellquad $^ ${PETSC_LIB}
	${DSYMUTIL} ellquad

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules